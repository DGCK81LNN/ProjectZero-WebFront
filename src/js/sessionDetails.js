

let details, errorCount = 0;

function initSession(accountPage = false, historyPage = false, adminPanel = false) {
    if(getSessionToken()) {
        mdui.$.ajax({
            url: apiUrl + '/api/session/getDetails',
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + getSessionToken()
            },
            error: () => {
                if(errorCount++ > 3) {
                    //removeSessionToken();
                    //window.location.href = '/login.html';
                }
                else {
                    setTimeout(() => initSession(accountPage, historyPage), 1000);
                }
            },
            success: (R) => {
                R = JSON.parse(R);
                details = R.data;
                if(!details.active) {
                    document.getElementById('active').innerHTML = '<div class="mdui-valign" style="border:1px solid hotpink;border-radius: 6px;height: auto;">' +
                        '<p class="mdui-typo" style="margin-left: 10px;margin-right: 10px;">账户尚未激活，请检查收件箱或垃圾箱并点击邮件中的链接以激活账户<div class="mdui-toolbar-spacer"></div><button onclick="reSendEmail()" class="mdui-btn mdui-btn-dense mdui-color-theme-accent mdui-ripple" style="margin-right: 10px;">重新发送</button></p>' +
                        '</div><br>';
                }
                if(accountPage) {
                    document.getElementById('emailShow').innerHTML = R.data.email;
                }
                if(!historyPage) {
                    document.getElementById('userGroup').innerHTML = R.data.role;
                    document.getElementById('userCredit').innerHTML = R.data.credit / 100;
                    if(R.useNAIFallback) document.getElementById('warntip').innerHTML = '当前图像生成处于 <a onclick="showFallback();" class="mdui-text-color-theme-accent">Fallback模式</a>'
                }
                if(adminPanel && R.data.role === 'Administrator') {
                    window.location.href = '/draw.html';
                }
                console.log(R);
            }
        });
    }
}

function showFallback() {
    mdui.dialog({
        title: 'NovelAI Fallback',
        content: `
                <div class="mdui-typo" style="text-align: center;">
                    当我们对Kamiya基础设施进行紧急维护时，您的图像生成请求将被转发至NovelAI以保证最低可用性。<br>
                    在该模式下您无法切换模型与生成Image2Image图像，并且无法查看图像生成进度。如果图像很久都没有完成生成则可以刷新页面发送新的请求，旧的请求在完成后将出现在 历史生成 中。
                </div>
                `
    });
}

function reSendEmail(callback) {
    newCaptchaPromise().then(({ token, type }) => {
        mdui.$.ajax({
            url: apiUrl + '/api/account/requestEmailVerification',
            method: 'POST',
            data: JSON.stringify({
                token: token,
                type: type
            }),
            contentType: 'application/json',
            headers: {
                'Authorization': 'Bearer ' + getSessionToken()
            },
            success: function () {
                mdui.snackbar('验证邮件已发送',{
                    position: 'right-top'
                });
                if(callback) callback();
            },
            error: function () {
                mdui.snackbar('验证邮件发送失败',{
                    position: 'right-top'
                });
                if(callback) callback();
            }
        });
    });
}

function checkIn() {
    /*
    newCaptchaPromise().then(({ token, type }) => {
        mdui.$.ajax({
            url: apiUrl + '/api/billing/checkin',
            method: 'POST',
            data: JSON.stringify({
                token: token,
                type: type
            }),
            contentType: 'application/json',
            headers: {
                'Authorization': 'Bearer ' + getSessionToken()
            },
            success: function (data) {
                data = JSON.parse(data);
                if(data.status === 200) {
                    mdui.snackbar('签到成功，' + data.message ,{
                        position: 'right-top'
                    });
                    initSession();
                }
                else {
                    mdui.snackbar('签到失败，' + data.message ,{
                        position: 'right-top'
                    });
                }
            },
            error: function () {
                mdui.snackbar('签到失败',{
                    position: 'right-top'
                });
            }
        });
    });
     */
    newCaptchaCallback(function (T, dialog) {
        dialog.close();
        mdui.$.ajax({
            url: apiUrl + '/api/billing/checkin',
            method: 'POST',
            data: JSON.stringify({
                token: T.token,
                type: T.type
            }),
            contentType: 'application/json',
            headers: {
                'Authorization': 'Bearer ' + getSessionToken()
            },
            success: function (data) {
                data = JSON.parse(data);
                if(data.status === 200) {
                    mdui.snackbar('签到成功，' + data.message ,{
                        position: 'right-top'
                    });
                    initSession();
                }
                else {
                    mdui.snackbar('签到失败，' + data.message ,{
                        position: 'right-top'
                    });
                }
            },
            error: function () {
                mdui.snackbar('签到失败',{
                    position: 'right-top'
                });
            }
        });
    });
}

function logout() {
    removeSessionToken();
    window.location.href = 'login.html';
}
